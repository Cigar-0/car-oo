package org.example;

public abstract class Vehicle {
    protected String name;
    protected int speed;
    protected String engine;
    protected int acc;
    public abstract void speedUp();

}
