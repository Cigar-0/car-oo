package org.example;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Cool car", 25, "electric");
//        car.speedUp();
        Truck truck = new Truck("Big Truck", 20, "gasoline");
//        truck.speedUpForTruck();
        Driver driver = new Driver();
        driver.drive(car);
        driver.drive(truck);

    }
}
