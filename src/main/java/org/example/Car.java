package org.example;

public class Car extends Vehicle{
//    private String name;
//    private int speed;

    public Car(String name, int speed, String engine) {
        this.name = name;
        this.speed = speed;
        this.engine = engine;
    }
    public void speedUp(){
        if(this.engine == "electric"){
            this.speed +=10;
        }else{
            this.speed +=5;
        }

//        this.speed +=5;
        System.out.printf("%s: speed up to %d km/h%n", this.name, this.speed);
    }


}
