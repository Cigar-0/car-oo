package org.example;

public class Driver {

    public void drive(Vehicle vehicle) {
        vehicle.speedUp();
    }
}
